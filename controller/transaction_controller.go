package controller

import (
	"financial-transaction-service/model"
	"financial-transaction-service/service"

	"net/http"

	"github.com/gin-gonic/gin"
)

func InitRoutes(router *gin.Engine) {
	// initiate route
	router.POST("/transaction", CreateTransaction)
	router.GET("/", GetAllTransaction)
}

func CreateTransaction(c *gin.Context) {
	var transactionRequest model.TransactionRequest

	// Bind JSON request body to the Transaction struct

	if err := c.ShouldBindJSON(&transactionRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Convert TransactionRequest to Transaction model
	transaction := model.Transaction{
		UserID: transactionRequest.UserID,
		Amount: transactionRequest.Amount,
		Type:   transactionRequest.Type,
	}

	// let's call a service to create a transaction
	if err := service.CreateTransaction(&transaction); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "failed to create a transaction"})
		return
	}
	c.JSON(http.StatusCreated, gin.H{"Message": "Transaction created"})

}

func GetAllTransaction(c *gin.Context) {
	transactions, err := service.GetAllTransactions()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"Error": "failed to retrieve transaction"})
		return
	}
	c.JSON(http.StatusOK, transactions)
	return

}
