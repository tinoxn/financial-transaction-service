package model

import "github.com/jinzhu/gorm"

// Transaction model to represent financial transactions
type Transaction struct {
	gorm.Model
	UserID string  `json:"user_id"`
	Amount float64 `json:"amount"`
	Type   string  `json:"type"` // credit or debit
}

type TransactionRequest struct {
	UserID string  `json:"user_id" binding:"required"`
	Amount float64 `json:"amount" binding:"required"`
	Type   string  `json:"type" binding:"required"`
}
