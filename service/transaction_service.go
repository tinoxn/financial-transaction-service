// service/transaction_service.go
package service

import (
	"financial-transaction-service/model"
	"log"

	"github.com/jinzhu/gorm"
)

var db *gorm.DB

func SetDB(database *gorm.DB) {
	db = database
}

func CreateTransaction(transaction *model.Transaction) error {
	if err := db.Create(transaction).Error; err != nil {
		log.Println("Error creating transaction:", err)
		return err
	}
	return nil
}

func GetTransactionHistory(userID uint) ([]model.Transaction, error) {
	// Implement transaction history retrieval logic
	return nil, nil
}

func GetAccountBalance(userID uint) (float64, error) {
	// Implement account balance retrieval logic
	return 0, nil
}

func GetAllTransactions() ([]model.Transaction, error) {
	var transactions []model.Transaction
	if err := db.Find(&transactions).Error; err != nil {
		return nil, err
	}
	return transactions, nil
}
