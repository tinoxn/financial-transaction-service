package migrations

import (
	"github.com/jinzhu/gorm"
)

func RunMigrations(db *gorm.DB) {
	if err := CreateTransactionsTable(db); err != nil {
		// Handle the error, you might want to log it or panic based on your needs
		panic(err)
	}
}
