package migrations

import (
	"financial-transaction-service/model"

	"github.com/jinzhu/gorm"
)

func CreateTransactionsTable(db *gorm.DB) error {

	err := db.AutoMigrate(&model.Transaction{}).Error
	if err != nil {
		return err
	}
	return nil
}
