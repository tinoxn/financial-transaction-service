package main

import (
	"financial-transaction-service/controller"
	"financial-transaction-service/migrations"
	"financial-transaction-service/model"
	"financial-transaction-service/service"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // Import the PostgreSQL driver
	_ "github.com/lib/pq"
)

var db *gorm.DB

const (
	host     = "localhost"
	port     = 5432
	user     = "tinox"
	password = "password"
	dbname   = "financial"
)

func init() {
	var err error
	db, err = gorm.Open("postgres", "host="+host+" port="+strconv.Itoa(port)+" user="+user+" dbname="+dbname+" sslmode=disable password="+password)
	if err != nil {
		panic(err)
	}

	// Automigrate your models (create tables)
	db.AutoMigrate(&model.Transaction{})
}

func migrate() {
	// Run all migrations
	migrations.RunMigrations(db)
}
func main() {
	defer db.Close()
	migrate()
	router := gin.Default()
	// Set up middleware before routes
	router.Use(func(c *gin.Context) {
		service.SetDB(db)
		c.Next()
	})
	controller.InitRoutes(router)
	router.Run(":8080")
}
